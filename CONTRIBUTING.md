
## how to lint the Dockerfile locally:

    docker run --rm -i hadolint/hadolint < Dockerfile

## how to build locally:

    docker build -t ansible-in-a-container:latest .


## how to prepare a new release

1. Update `VERSION` file with new version number.
2. Create file `CHANGELOGS/v<version>.md` with the filename of new version and fill in the changes.
3. Merge commits into default branch or push commit to default branch to trigger the release pipeline.
