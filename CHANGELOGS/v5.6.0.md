
### BREAKING CHANGES

- changed to rootless baseimage. See Release [v5.5.0-rootless](https://gitlab.com/das-floss/ansible-in-a-container/-/releases/v5.5.0-rootless) or !1

### Added

- Added environment variables `ANSIBLE_CONFIG=ansible.cfg` and `ANSIBLE_FORCE_COLOR=true` to image

### Updated

- updated ansible to v5.6.0
- updated awscli to v1.22.91
