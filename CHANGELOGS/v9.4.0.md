### Updated

- Update ansible to v9.4.0
- Update base-image `docker` to v24.0.9

#### Updated tools

- Update ansible-lint to 24.2.1
- Update awscli to v1.32.72
- Update helm to v3.14.3
- Update helm-diff to v3.9.5
- Update kubectl to v1.29.3
- Update session-manager-plugin to v1.2.553.0

#### Updated dependencies

- Update dependency bcrypt to v4.1.2
- Update dependency boto3 to v1.34.3
- Update dependency botocore to v1.34.72
- Update dependency netaddr to v0.10.1
