### Updated

- updated ansible to 5.4.0
- updated awscli to 1.22.61
- updated cfn-lint to 0.58.1
- updated kubectl to 1.23.4
