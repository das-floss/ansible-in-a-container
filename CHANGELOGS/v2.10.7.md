Added:

- Create GitLab Release via Pipeline Job

Updated:

- ansible to **2.10.7**
- awscli to **1.19.9**
- ansible-lint to **5.0.0**
- cfn-lint to **0.45.0**
- kubectl to **1.20.2**

Fixed:

- pinned `cryptography` to **3.3.1** due error while installing version 3.4
