### Updated

- update ansible to 4.6.0
- update ansible-lint to 5.2.0
- update awscli to 1.20.59
- update cfn-lint to 0.54.2
- update helm to 3.7.0
- update kubectl to 1.22.2

### Fixed

- set Berlin timezone to get this date+time in gitlab pipelines.
