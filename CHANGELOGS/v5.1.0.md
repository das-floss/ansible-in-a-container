### Updated

- update ansible to 5.1.0
- update ansible-lint to 5.3.1
- update awscli to 1.22.26
- update cfn-lint to 0.56.3
- update helm to 3.7.2
- update kubectl to v1.23.1
