################################################################################
# downloader stage via curl image
# For updates see https://hub.docker.com/r/curlimages/curl
FROM --platform=${TARGETPLATFORM} curlimages/curl:8.8.0@sha256:73e4d532ea62d7505c5865b517d3704966ffe916609bedc22af6833dc9969bcd as downloader
ARG TARGETARCH

# set to a writeable directory:
WORKDIR /home/curl_user/
# fail if any command in a pipe fails. See https://github.com/hadolint/hadolint/wiki/DL4006
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]


# Download helm
# See https://github.com/helm/helm/releases
# renovate: datasource=github-releases depName=helm/helm
ENV HELM_VERSION=v3.15.1
# hadolint ignore=SC3009
RUN curl --location --fail --remote-name https://get.helm.sh/helm-${HELM_VERSION}-linux-${TARGETARCH}.tar.gz{,.sha256sum} && \
    sha256sum -c helm-${HELM_VERSION}-linux-${TARGETARCH}.tar.gz.sha256sum && \
    tar xzf helm-${HELM_VERSION}-linux-${TARGETARCH}.tar.gz linux-${TARGETARCH}/helm


# Download kubectl
# Get latest available Version with: curl -Ls https://dl.k8s.io/release/stable.txt
# See https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
# renovate: datasource=github-releases depName=kubernetes/kubernetes extractVersion=^v(?<version>.*)$
ENV KUBECTL_VERSION=1.31.2
# hadolint ignore=SC3009
# hadolint ignore=SC2059
RUN curl --location --fail --remote-name https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/linux/${TARGETARCH}/kubectl{,.sha256} && \
    printf "$(cat kubectl.sha256)  kubectl\n" | sha256sum -c

################################################################################
# Use a Golang image to build the desired version of the AWS ssm plugin
FROM --platform=${TARGETPLATFORM} golang:1.17-alpine AS ssm-plugin-builder
ARG TARGETARCH
# See https://github.com/aws/session-manager-plugin/releases
ARG SSM_PLUGIN_VERSION=1.2.553.0

RUN apk --no-cache add \
        tar=1.34-r1 \
        gzip=1.12-r0 \
        wget=1.21.3-r0 \
        zip=3.0-r9 \
        git=2.36.6-r0 \
        make=4.3-r0 \
        bash=5.1.16-r2 \
        gcc=11.2.1_git20220219-r2 \
        musl-dev=1.2.3-r3 \
        gcompat=1.0.0-r4

WORKDIR /go/session-manager-plugin

RUN printf "" \
  && if [ "${TARGETARCH}" = "arm64" ]; then export MAKE_COMMAND="build" ; else export MAKE_COMMAND="build-linux"; fi \
  && git clone https://github.com/aws/session-manager-plugin.git . \
  && git checkout ${SSM_PLUGIN_VERSION} \
  && make clean pre-release "${MAKE_COMMAND}-${TARGETARCH}" \
  && cp "/go/session-manager-plugin/bin/linux_${TARGETARCH}_plugin/session-manager-plugin" /session-manager-plugin \
  && printf ""


################################################################################
# Use Docker in Docker Image to allow docker commands in pipeline jobs:
# For updates see https://hub.docker.com/_/docker
FROM --platform=${TARGETPLATFORM} docker:20.10.24-dind-rootless@sha256:cadf2fb68d9623c4ed8e1e20f5e388d4b736ac55f483e72d0ceb3a734d1a30db
ARG TARGETARCH

# to allow installations in rootless image:
USER root

COPY --from=downloader /home/curl_user/linux-${TARGETARCH}/helm /usr/local/bin/helm
COPY --from=downloader /home/curl_user/kubectl /usr/local/bin/kubectl
COPY --from=ssm-plugin-builder /session-manager-plugin /usr/local/bin
RUN chmod +x /usr/local/bin/kubectl

# build packages required by pip to install some ansible dependencies
ENV BUILD_PACKAGES \
  python3-dev \
  build-base \
  musl-dev \
  libffi-dev \
  openssl-dev \
  openssl

# list of packages to install that will be kept
ENV INSTALL_PACKAGES \
  py3-pip \
  python3 \
  py3-netaddr \
  ca-certificates \
  openssh-client \
  git \
  subversion \
  rsync \
  curl \
  jq \
  bash \
  tzdata

# define timezone to get correct time+date in gitlab pipelines
ENV TZ=Europe/Berlin

WORKDIR /installation

COPY requirements.txt .

# hadolint ignore=DL3013,DL3018
RUN apk --no-cache add ${BUILD_PACKAGES} ${INSTALL_PACKAGES} && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install --no-cache-dir --requirement requirements.txt && \
    rm -rfv /root/.cache && \
    rm -rfv /var/cache/apk/* && \
    apk del --rdepends ${BUILD_PACKAGES}

# Fix permissions to install helm plugin
RUN chown -R rootless:rootless /home/rootless/.local

USER rootless:rootless

# set to a writeable directory:
WORKDIR /home/rootless/

# install helm-diff. needs git for installation.
# renovate: datasource=github-releases depName=databus23/helm-diff
ENV HELM_DIFF_VERSION=v3.9.11
RUN helm plugin install https://github.com/databus23/helm-diff --version $HELM_DIFF_VERSION

# Defining ANSIBLE_CONFIG (even with the default value) fixes the error:
# "Ansible is being run in a world writable directory" in GitLab pipelines.
# See https://docs.ansible.com/ansible/latest/reference_appendices/config.html#avoiding-security-risks-with-ansible-cfg-in-the-current-directory
ENV ANSIBLE_CONFIG=ansible.cfg
# forces color in GitLab pipelines
ENV ANSIBLE_FORCE_COLOR=true
