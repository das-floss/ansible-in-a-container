# ansible-in-a-container

[![Latest Release](https://gitlab.com/das-floss/ansible-in-a-container/-/badges/release.svg)](https://gitlab.com/das-floss/ansible-in-a-container/-/releases)

## description

A Docker container based on [`docker:dind-rootless`](https://hub.docker.com/_/docker) which includes Ansible and other tools like awscli, kubectl, helm, git and ansible/cfn-lint for usage in GitLab pipelines.

The releases of `ansible-in-a-container` match the same version like the included Ansible Version. For patch release of `ansible-in-a-container` not related to Ansible, the a minus sign is appended (e.g. `v5.7.0-1`) .See https://semver.org/#spec-item-10

## get the image

We've build a pipeline that builds images and pushes it to the public GitLab.com container registry of this project (see [.gitlab-ci.yml](.gitlab-ci.yml)).

Just run: `docker pull registry.gitlab.com/das-floss/ansible-in-a-container:v9.6.0`

## use it in your GitLab pipeline

Add following to your `.gitlab-ci.yml`

just for one pipeline job:

```yaml
deploy-job:
  stage: deploy
  image: registry.gitlab.com/das-floss/ansible-in-a-container:v9.6.0
  script:
    ...
```

or to use it for all of your pipeline jobs:

```yaml
default:
  image: registry.gitlab.com/das-floss/ansible-in-a-container:v9.6.0
```

# how to run the image ...

##  ... to check the version:

    docker run --rm -it registry.gitlab.com/das-floss/ansible-in-a-container:latest ansible --version
    ansible [core 2.12.4]
      config file = None
      configured module search path = ['/root/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
      ansible python module location = /usr/lib/python3.9/site-packages/ansible
      ansible collection location = /root/.ansible/collections:/usr/share/ansible/collections
      executable location = /usr/bin/ansible
      python version = 3.9.7 (default, Nov 24 2021, 21:15:59) [GCC 10.3.1 20211027]
      jinja version = 3.1.1
      libyaml = False

## ... with your aws credentials configured in ~/.aws

    docker run --rm -it -w /ansible -v $(PWD):/ansible -v ~/.aws:/root/.aws/ -e AWS_PROFILE=XYZ registry.gitlab.com/das-floss/ansible-in-a-container:latest ansible-playbook myplaybook.yaml -e @variables.yaml
